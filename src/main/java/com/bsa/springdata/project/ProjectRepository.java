package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query("select t.project FROM Team t where t.technology.name = :technology order by t.users.size")
    List<Project> findTop5ByTechnology(String technology, Pageable pageable);

    @Query("select t.project from Team t order by t.project.teams.size DESC, t.users.size DESC, t.project.name DESC ")
    List<Project> findTheBiggest(Pageable pageable);

    // ☺ ☺ ☺
    // TODO Fix this
    @Query(value = "select p.name," +
            "(select count(t) from teams t where t.project_id = p.id), " +
            "(select sum(count(t)) from teams t where t.project_id = p.id), " +
            "(select count(tech.id) from Technology tech ) " +
            "from projects p",
            nativeQuery = true)
    List<ProjectSummaryDto> getSummary();

    // impossible to understand how to work with ManyToMany
    @Query("select count(p) from Project p where p IN (select u.team.project from User u where u.roles.size > 0)")
    int getCountWithRole(String role);
}