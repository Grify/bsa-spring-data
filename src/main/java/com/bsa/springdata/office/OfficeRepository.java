package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    // TODO use only one query
    @Query("select o from Office o where o IN (select u.office from User u where u.team.technology.name = :technology)")
    List<Office> findAllByTechnology(String technology);

    @Modifying
    @Query("update Office o set o.address = :newAddress where o.address = :oldAddress AND o.users.size > 0")
    void updateAddress(String oldAddress, String newAddress);

    Optional<Office> findByAddress(String address);
}
