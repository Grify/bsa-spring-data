package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    @Transactional
    @Modifying
    @Query("update Team t set t.technology =" +
            "(select tech from Technology tech where tech.name = :newTechnologyName) " +
            "where t.technology IN (select tech from Technology tech where tech.name = :oldTechnologyName)" +
            "and t.users.size < :devsNumber")
    void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName);

    @Transactional
    @Modifying
    @Query(value = "UPDATE teams SET name = concat(" +
            ":hipsters, '_', " +
            "(SELECT name FROM projects WHERE projects.id IN (SELECT project_id FROM teams WHERE name = :hipsters))," +
            "'_'," +
            "(SELECT name FROM technologies WHERE technologies.id IN (SELECT technology_id FROM teams WHERE name = :hipsters)))" +
            "WHERE name = :hipsters",
            nativeQuery = true)
    void normalizeName(String hipsters);

    Optional<Team> findByName(String name);
    int countByTechnologyName(String name);
}
