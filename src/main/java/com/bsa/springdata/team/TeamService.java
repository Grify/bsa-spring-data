package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        // TODO: You can use several queries here. Try to keep it as simple as possible
        // описать логику обновления технологии у команды, в которой участников < определенного
        // количества. Можно использовать несколько запросов
        teamRepository.updateTechnology(devsNumber, oldTechnologyName, newTechnologyName);
    }

    public void normalizeName(String hipsters) {
        // TODO: Use a single query. You need to create a native query
        // описать один запрос, который будет добавлять к имени указанной команды название проекта и
        // название технологии (Team_Project_Technology). Необходимо использовать nativeQuery
        teamRepository.normalizeName(hipsters);
    }
}
