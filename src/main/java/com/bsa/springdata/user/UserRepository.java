package com.bsa.springdata.user;

import com.bsa.springdata.user.dto.UserDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findAllByLastNameContainingIgnoreCase(String lastName, Pageable pageable);

    @Query("select u from User u where u.office.city = :city order by u.lastName asc")
    List<User> findAllByCity(String city);

    List<User> findAllByExperienceIsGreaterThanEqual(int experience, Sort sort);

    @Query("select u from User u where u.office.city = :city and u.team.room = :room")
    List<User> findAllByRoomAndCity(String city, String room, Sort sort);

    @Modifying
    @Query("delete User u where u.experience < :experience")
    int deleteAllByExperience(int experience);
}
